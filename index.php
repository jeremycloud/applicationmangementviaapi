<?php
include 'CloudwaysAPIClient.php';
$api_key = 'YOUR API KEY';
$email = 'YOUR EMAIL';
$cw_api = new CloudwaysAPIClient($email,$api_key);
$servers = $cw_api->get_servers();
$apps = [];
$success = null;
if(!empty($_POST)){
  $server = $_POST['server'];
  $appname = $_POST['app'];
  $re = $cw_api->DeleteApplication($server,$appname);
  if(isset($re->operation_id)){
    $success = "Appication is being Deleted";
  }
  else{
    $success = $re->message;
  }
}
?>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
      Application
    </title>
    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-10">
          <form class="form-horizontal" method="post" action="
<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
            <fieldset>
              <!-- Form Name -->
              <legend>Delete Application</legend>
              <!-- Select Basic -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="server">Server</label>
                <div class="col-md-4">
                  <select id="server" name="server" class="form-control">
                    <option value="">Select Your Server</option>
                    <?php 
foreach($servers->servers as $server) {
  echo "<option value='".$server->id."'>".$server->label."</option>";
}
                    ?>
                  </select>
                </div>
              </div>
              <!-- Select Basic -->
              <div class="form-group">
                <label class="col-md-4 control-label" for="application">Application</label>
                <div class="col-md-4">
                  <select id="app" name="app" class="form-control disable">
                  </select>
                </div>
              </div>
              <!-- Button -->
              <div class="form-group">
                <label class="col-md-4 control-label" for=""></label>
                <div class="col-md-4">
                  <button id="" name="" class="btn btn-danger">Delete</button>
                  <span class="bg-success"><?php echo $success  ?> </span>
                </div>
              </div>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $( "#server" ).change(function() {
        val = $(this).val();
        switch(val) {
            <?php foreach($servers->servers as $server) { ?>
          case <?php echo "\"".$server->id."\""; ?>:
            $('#app')
            .find('option')
            .remove()
            .end();
            <?php $i ="<option value=''>Select Your Application</option>"; 
                                   foreach ($server->apps as $app) {
                                     $i .= "<option value='".$app->id."'>".$app->label." (".$app->application.")</option>";
                                     $i++;
                                   }?>
            apps ="<?php echo $i;?>";
            $('#app').html(apps);
            break;
            <?php } ?>
          default:
            $('#app')
            .find('option')
            .remove()
            .end();
            break;
        }
      }
                 );
    </script>
  </body>
</html>